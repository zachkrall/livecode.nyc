# Network

Live-coding and Algorave are global movements. Below is a list of known communities of practice for live-coding.

New York City:

- [Music Community Lab](http://musiccommunitylab.org/)
- [Phase Space](https://phasespace.nyc/)

<br/>

Global:

- [TOPLAP](https://toplap.org)
- [ICLC (International Conference on Live Coding)](https://iclc.toplap.org/)
- [Algorave](https://algorave.com/)
- [Eulerroom](https://youtube.com/eulerroom)

<br/>

North America:

- United States
  - [AV Club SF](https://avclubsf.com/) (San Francisco)
  - [Live Code RVA](https://twitter.com/livecodeRVA) (Virginia)
- Mexico
  - [TOPLAP México](https://www.facebook.com/toplap.mx/)
- Canada
  - [Cybernetic Orchestra](https://www.facebook.com/CyberneticOrchestra/) (Ontario)
  - [Live Coding à Montréal](https://montreal.toplap.org/) (Montreal)

<br/>

South America:

- [CLiC (Colectivo de Live Coders)](https://colectivo-de-livecoders.gitlab.io/) (AR)
- [Live Coding @ IMPA](http://w3.impa.br/~vitorgr/livecode/) (Rio De Janerio, BR)
- [TOPLAP Quito](https://www.facebook.com/groups/583681711997021/) (EC)
- [TOPLAP Lima](https://www.facebook.com/groups/362002184270964/) (PE)
- [TOPLAP Valdiva](https://toplapvaldivia.wixsite.com/website) (CL)
- Colombia
  - [TOPLAP Medillín](https://www.facebook.com/groups/626111581071250/)
  - [TOPLAP Bogotá](https://www.facebook.com/groups/626111581071250/)

<br/>

Asia:

- [Algorave India](https://algoraveindia.github.io/)
- [TOPLAP Japan](https://twitter.com/toplapjp)
- [TOPLAP Shanghai](https://www.facebook.com/TOPLAPSH/)

<br/>

Europe:

- [Algoritmi Torino](https://www.facebook.com/AlgoritmiTorino) (IT)
- [TOPLAP Barcelona](https://toplapbarcelona.hangar.org/) (ES)
- [TOPLAP Berlin](https://www.facebook.com/groups/toplapnodeberlin/1725919034313087/) (DE)
- [NC_CL (Netherlands Coding Live)](https://netherlands-coding-live.github.io/) (NL)
- [TOPLAP Italia](https://www.facebook.com/groups/1051671308353969/)
- [TOPLAP France](https://www.facebook.com/groups/toplapfr/)
- [LiveCoding Düsseldorf](https://www.facebook.com/groups/587715001671363/)
- UK
  - [TOPLAP Yorkshire](https://www.facebook.com/groups/1683408058575303/)
  - [TOPLAP North-East](https://www.facebook.com/groups/897471030365142/)
  - [Tidalclub Sheffield](https://tidalclub.github.io/sheffield.html)

<br/>

We recognize that this list is not comprehensive. If you'd like to be added, let us know!

<br/>

Old Friends. Lost Connections. Communities that might be still out there:
- North America
  - Live Code New England
  - Live Code Pittsburgh
- Asia 
  - TOPLAP Taiwan
  - TOPLAP Isreal
- Europe
  - Algorave France & Belgique
  - Live Coding Frankfurt
