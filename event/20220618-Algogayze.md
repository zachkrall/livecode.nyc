---
title: Algogayze
location: Wonderville, 1186 Broadway, Brooklyn, NY 11221
date: 2022-06-18 8:00 PM
duration: 3:30
image: /image/06182022_algogayze.jpg
categories: [algorave, pride, lgbtq+, lgbt, code, music, art, computers]
link: https://withfriends.co/event/14498572/algogayze 
---

# ALGOGAYZE

Livecode.nyc presents an all lgbtq+ identified lineup in celebration of prid month!
Save the date, more details TBA

