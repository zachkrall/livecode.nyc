---
title: Wonderverse Algorave
location: Wonderverse
date: 2020-06-20 4:00 PM
image: /image/2020-wonderverse.jpg
---

Wonderville is teaming up with LiveCodeNYC for a Minecraft Algorave! Players can buy tickets to the server for $5 or watch for free on the Wonderville Twitch channel:

https://www.twitch.tv/wonderville

To join the server, you'll need a copy of Minecraft Java Edition. You can buy it here for $27:

https://www.minecraft.net/en-us/store/minecraft-java-edition

Featuring sets by:
* Electric Detectives: Nancy Drone & Hue Archer
* Em DeGrandpe & Char Stiles
* amitlzkpa & deadf00f
* mgs.nyc
* Obi Wan Codenobi & The Wookie
* Colonel Panix & Bliss Factory
* Johnlp.xyz & Messica Arson
* Melody Loveless & Laelume
* Solquemal & Dan Gorelick
* with lofi hip hop algorithmic beats by Reckoner

Proceeds will be matched and donated to POWRPLNT, a digital art space for teens to learn digital skills, their mission is to reach under-resourced communities in Bushwick, Brooklyn.

To learn more about POWRPLNT, visit: [powrplnt.org](http://powrplnt.org)
