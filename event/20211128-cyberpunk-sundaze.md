---
title: Cyberpunk Sundaze
location: wonderville
date: 2021-10-08 8:00 PM
link: https://withfriends.co/event/13060483/cyberpunk_sundaze
image: /image/2021-11-28-cyberpunk_sundaze.png
---

### Description
The year is 202X. Cyber Monday has ravaged Earth. Corporate algorithms have diminished humanity's sense of free will. Mark Zuckerberg is still smoking meat. All that remains is a ragtag group of 14 performers harnessing the very same tools used for surveillance capitalism to instead unleash audio visual experiences that ring true in their hearts.

Cyberpunk Sundaze is a celebration of gloom & doom aesthetics, the meaning of friendship, and the power of dance or something!

### Music & Vis
zzwalsbyi & Islands of Stability
Colonel Panix & Edgardo
nitchafame & kengchakaj
Naltroc & archaic.technology
CasualSalad & Indira
Natalie & Phoebe
Doc Mofo & LeHank
