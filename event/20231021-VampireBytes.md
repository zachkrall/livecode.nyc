---
title: "Night of the Living Code II: Vampire Bytes"
location: Wonderville, 1186 Broadway, Brooklyn, NY 11221
date: 2023-10-21 7:00 PM
duration: 7:00
image: /image/VampireBytesPosterV5Square.png
categories: [algorave, music, art, computers]
---

# Night of the Living Code II: Vampire Bytes

LiveCodeNYC Presents… Night of the Living Code II: Vampire Bytes! 🧛‍♀️

Saturday, October 21st, Wonderville and LiveCodeNYC come together again for a night of chills and thrills as performers spellbind you with their live coded audio and visual creations. You’ll see apparitions in the pixels 👻, hear the moans of zombies crackle in the audio 🧟, and, since it’s live, most likely a few “bugs” that will make your skin crawl 🐛

Doors open at 7pm, join us if you dare!
21+ to enter, Masks Required

Dancing 🎵, Halloween vibes 🎃, and performances by:

Varsity Star [(@varsity.star)](https://instagr.am/varsity.star)

Marzipan [(@negaverse_buffet)](https://instagr.am/negaverse_buffet)

Azhad Syed [(@azhadsyed)](https://instagr.am/azhadsyed)

MrSynAckster [(@mrsynackster)](https://instagr.am/mrsynackster)

Dog Collar [(@dogxcollar)](https://instagr.am/dogxcollar)

Xenon Chameleon [(@xenon_chameleon)](https://instagr.am/xenon_chameleon)

VampireExec [(@vampireexec)](https://instagr.am/vampireexec)

Cougars Are Cats Too [(@katarina_hoeger_art)](https://instagr.am/katarina_hoeger_art)

kastakila [(@kasta_kila)](https://instagr.am/kasta_kila)

Missy [(@missy222222222)](https://instagr.am/missy222222222)

Night Shining  [(@_nightshining)](https://instagr.am/_nightshining)

naltroc [(@naltroc)](https://instagr.am/naltroc)

genesis rogue [(@supernaturalbxh)](https://instagr.am/supernaturalbxh)

f00f [(https://f00f.live)](https://f00f.live)
