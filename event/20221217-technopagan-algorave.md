---
title: Technopagan Solstice Algorave
location: Wonderville, 1186 Broadway, Brooklyn, NY 11221
date: 2022-12-17 8:00 PM
duration: 4:30
image: /image/2022_TechnopaganSolstice_Wide.png
categories: [algorave, music, art, computers, technopagan, solstice]
link: https://withfriends.co/event/15383456/technopagan_solstice_algorave
---

# Technopagan Solstice Rave

LivecodeNYC is filling the midwinter night with arcane computer magic. The world is dark and cold, but we'll keep warm with our revelry and overheating GPUs. Come dance and celebrate the coming year with chaotic code-based beats and visions. Featuring sets by:

## Lineup

[Brian Abelson](https://www.instagram.com/abelsonlive/) + [shellylynnx](https://www.instagram.com/shellylynnx/)

[MDN](https://www.instagram.com/markdenardo/)

[Archaic](https://www.instagram.com/archaic.tech/) [Reckoner](https://www.instagram.com/reckonermusic/) + [IV](https://www.ivpravdin.com/)

[Justin Swirbel](https://www.instagram.com/jswirbs/) + [LAVA](https://www.instagram.com/luckygolava/)

[Melody Loveless](https://www.instagram.com/melodycodes/) + [Voyde](https://www.instagram.com/voydescreamsback/)

[Messica Arson](https://www.instagram.com/messicaarson/)
