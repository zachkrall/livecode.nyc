---
title: Source Fest Algorave
location: Brooklyn Bazaar, 165 Banker St, Brooklyn, NY 11222
date: 2017-05-21 8:00 PM
link: https://www.facebook.com/events/188760088299401
---

LiveCode.NYC presents

Source ALGORAVE


Sounds:

xname

Scorpion Mouse

2050

Parrises Squares


Visuals + Games:

Ulysses Popple

Ramsey Nasser + Tims Gardner

+MORE TBC

SourceFestival…

Join LiveCode.NYC and NYU's IDM for 3 days of performances, discussions, workshops, and of course and algorave, all centered around the practice of Live Coding. Music, visuals and even dance will be created with interactive systems as part of a festival celebrating coding in real time.


LiveCode.NYC...

We are a meet up group that focuses on the practice of live coding. We run several kinds of events. We like to say, "We are language agnostic and not medium specific. We live code games in LISP, music in Extempore, dance in Java, and everything in between. We program systems as they are running. Life changes in real-time and so does our code. Livecode.NYC.”

