---
title: Queer Coded
location: Wonderville, 1186 Broadway, Brooklyn, NY 11221
date: 2023-06-17 8:00 PM
duration: 5:00
image: /image/QUEER-CODED-1080x1080.png
categories: [algorave, music, art, computers, pride, queer, DJ]
link: https://withfriends.co/event/16251214/queer_coded
---

# QUEER CODED✧⊹QUEER CODED⊹✧⊹QUEER CODED⊹✧

The night is gay, the month is Pride, and the event is QUEER CODED! LiveCode.NYC takes over Wonderville for a trans & nonbinary-forward, queer-centric livecoding show. It will be dancy and spooky and weird and beautiful, and everyone's invited. Load a fabulous outfit, corrupt your gender binaries and come party with us!

😷 Masks Required 😷


🎶Music + 📺Visuals by:

&RU + sandpills
PRESWERVE + this.xor.that
Melody Loveless + starlybri
MDN
Borbo + PLAY
Luciform + snow\_angel
MadaME Lovelace + Dog Collar

💿DJs in the bar:

f00f
P0L4R1S
archaic.tech
