---
title: '& Friends Performances'
location: Brooklyn Art Haus
date: 2023-12-02 7:00 PM
duration: 4:00
link: https://www.stellartickets.com/o/brooklyn-art-haus--2/events/and-friends/occurrences/b17d6ebf-74e7-4b10-96fc-7d071bc259b4
image: '/image/&friends.png'
categories: [algorave, music, art, computers]
feed:
    enable: true
---

Welcome to "& Friends," an audio-visual experience that goes beyond traditional event boundaries. This gathering serves as a unique platform for artists in the vibrant city of New York who share a passion for performances centered around visual art, pushing the limits of art and technology with code & music.


Embark on a journey with us as we welcome AlbertData from Barcelona on tour. AlbertData will present a live A/V set titled “Slowly Fading into Data ” part of a speculative project expressed in various artistic formats such as Retro-Game, Music Album, Live A/V Performance, Experimental Film, and Arcade Installation. This immersive experience stems from AlbertDATA’s exploration of disembodiment, extended cognition, hybrid beings, and synthetic identities. Joining AlbertData are the Glad Scientist and a livecodenyc curation featuring alsoknownasrox, kastakila & V10101A, Kevin Peter He & Jake Oleson, Voyde & Xenon Chameleon, Carnel Ex, Mfclarke and MDN.


Dive into this dynamic community, forge connections with fellow artists and delve into the intriguing intersection of creativity and technology.


Brought to you by Creative Code Art, livecodenyc & AlbertDATA Note: We have a no turn away policy for those that can't afford to purchase a ticket. Please email:

creativecodeart@gmail.com

to request a spot. Tickets for the workshops & artist talk are sold separately here: [https://shotgun.live/events/friends-workshops](https://shotgun.live/events/friends-workshops)
