---
title: Regen Circuit Day 7
location: Harvestworks Art and Technology Program Building 10a, Nolan Park, Governors Island 
date: 2023-05-12 11:00 AM
duration: 5:45
image: /image/2023-RegenCircuit_Banner.png
link: https://www.harvestworks.org/apr-28-may-14-livecode-nyc-residency-at-the-harvestworks-art-and-technology-program-on-governors-island/
categories: [algorave, music, art, computers]
---

# Regen Circuit
## LiveCode.NYC Residency at the Harvestworks Art and Technology Program on Governors Island

The event will be open to the public on Fridays, Saturdays, and Sundays from April 28th to May 14th from 11:00AM to 4:45PM.

Regen Circuit is an art and tech exhibition featuring LiveCode.NYC, a social group and artist collective dedicated to live coding. During their residency at the Harvestworks Art and Technology Building, members will host performances, conduct workshops and presentations, and showcase art. Installation, video, sculpture, and more will be on display. Regen Circuit aims to bring people together to celebrate the art of live coding while creating moments to connect and reflect on the efforts and visions of people in our community.

**Participants include:** Archaic Reckoner (Sumanth Srinivasan + Matthew Kaney), Edgardo Avilés-López, Max Bennett, casualsalad (Chirag Davé), Omar Delarosa, Sarika Doppalapudi, ele-khle-kha อีเหละเขละขละ (Kengchakaj Kengkarnka and Nitcha Tothong), emptyflash (Cameron Alexander), Carla Guzman, Riho Hagi, Roxanne Harris, Viola He, Katarina Hoeger, Sylvia Ke, LAVA (Nico Perey), Melody Loveless, Daniel McKemie, Messica Arson (Jessica Garson), Mister Bomb (Liam Baum), MYLAR (Caitlin Cawley and Melody Loveless), Ramsey Nasser, PRESWERVE, Snow Schwartz, Kate Sicchio, Sabrina Sims, Michael Simpson, thisxorthat (Jessica Stringham), Jay Tobin, Loren Tyler, Voyde (Indira Ardolic), Emma Waddell, Shelly Xiong, and Andrew Yoon.

This is a [Satellite Event](https://iclc.toplap.org/2023/satellite.html) for this year’s [International Conference of Live Coding](https://iclc.toplap.org/2023/index.html)
Event organized by Melody Loveless
