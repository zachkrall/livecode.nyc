---
name: Eric Lee (Easterner)
image: https://live.staticflickr.com/65535/52733737725_47ec950693_k.jpg
links:
    instagram: https://www.instagram.com/eggsovereager/
---

Experimental audiovisual artist. Glitches old and new technologies--ubiquitous to obsolete--to explore their poetic potential, and critique their roles in society.