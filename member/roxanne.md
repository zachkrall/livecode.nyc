---
name: Roxanne Harris
image: https://s3.us-west-2.amazonaws.com/secure.notion-static.com/e73e7fbf-6993-46ce-857d-73d6ad8fe822/43443979_1919236451544030_7575798577536958464_n.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20220224%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20220224T004236Z&X-Amz-Expires=86400&X-Amz-Signature=1d62285272a3403acee6f4a7ccba04ed0b89429e646096d28d4c88fc629e6cc2&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%2243443979_1919236451544030_7575798577536958464_n.jpg%22&x-id=GetObject
links:
    website: https://mulberry-sesame-fab.notion.site/Roxanne-Harris-2d8771a2e53142dbad3edac4af23b716
    instagram: https://www.instagram.com/alsoknownasrox/
    soundcloud: https://soundcloud.com/user-142880218?utm_source=clipboard&utm_medium=text&utm_campaign=social_sharing
    youtube: https://www.youtube.com/channel/UC5qhpJyFzpyVfQrua70lPVQ
---

I make music by programming on-the-fly. I am a new artist wanting to increase exposure to the practice of live coding and looking for performance opportunities to share my music.